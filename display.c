/*
Copyright (c) 2021, Galen Nare, Ahila Moorthy
All rights reserved.
*/

#include "display.h"
void display_time(int hours, int minutes, int seconds, pi_framebuffer_t *dev){
    //display time on device dev using the below helpers
    clearFrameBuffer(dev, BLACK);
    display_colons(dev);
    display_hours(hours%24, dev);
    display_minutes(minutes%60, dev);
    display_seconds(seconds%60, dev);
}

void display_colons(pi_framebuffer_t*dev){
    sense_fb_bitmap_t *bm = dev -> bitmap;

    bm -> pixel[1][1] = WHITE;
    bm -> pixel[1][2] = WHITE;
    bm -> pixel[2][1] = WHITE;
    bm -> pixel[2][2] = WHITE;
    bm -> pixel[4][1] = WHITE;
    bm -> pixel[4][2] = WHITE;
    bm -> pixel[5][1] = WHITE;
    bm -> pixel[5][2] = WHITE;
    bm -> pixel[1][4] = WHITE;
    bm -> pixel[1][5] = WHITE;
    bm -> pixel[2][4] = WHITE;
    bm -> pixel[2][5] = WHITE;
    bm -> pixel[4][4] = WHITE;
    bm -> pixel[4][5] = WHITE;
    bm -> pixel[5][4] = WHITE;
    bm -> pixel[5][5] = WHITE;
}

void display_hours(int hours,pi_framebuffer_t*dev){
    sense_fb_bitmap_t *bm = dev -> bitmap;
    for (int i = 0; i < 6; i++) bm->pixel[i][6] = hours >> i & 1 ? BLUE : BLACK;
}

void display_minutes(int minutes,pi_framebuffer_t*dev){
    sense_fb_bitmap_t *bm=dev->bitmap;

    for (int i = 0; i < 6; i++) bm->pixel[i][3] = minutes >> i & 1 ? GREEN : BLACK;
}

void display_seconds(int seconds,pi_framebuffer_t*dev) {
    sense_fb_bitmap_t *bm = dev -> bitmap;
    for (int i = 0; i < 6; i++) bm->pixel[i][0] = seconds >> i & 1 ? RED : BLACK;

}
