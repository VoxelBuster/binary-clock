/*
Copyright (c) 2021, Galen Nare, Ahila Moorthy
All rights reserved.
*/

#include "display.h"
#include <signal.h>

volatile int running = 1;

void sigHandler(int sig) {
    running = 0;
}

int main(void) {
    pi_framebuffer_t *device = getFrameBuffer();
    
    signal(SIGINT, sigHandler);

    while (running) {
	int h, m, s;
	int code = scanf("%d:%d:%d", &h, &m, &s);
        printf("%d", code);
        if (code != 3 || h < 0 || 24 < h || m < 0 || 59 < m || s < 0 || 59 < s) running = 0;
	display_time(h, m, s, device);
    }
    
    clearFrameBuffer(device, BLACK);
    freeFrameBuffer(device);
}
