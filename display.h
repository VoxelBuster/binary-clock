/*
Copyright (c) 2021, Galen Nare, Ahila Moorthy
All rights reserved.
*/

#include <stdio.h>
#include <stdlib.h>
#include "sense.h"
#include <math.h>

#define RED   getColor(0xff,0x00,0x00)
#define GREEN getColor(0x00,0xff,0x00)
#define BLUE  getColor(0x00,0x00,0xff)
#define WHITE getColor(0xff,0xff,0xff)
#define BLACK getColor(0x00,0x00,0x00)

void display_time(int, int, int, pi_framebuffer_t*);
void display_colons(pi_framebuffer_t*);
void display_hours(int, pi_framebuffer_t*);
void display_minutes(int, pi_framebuffer_t*);
void display_seconds(int, pi_framebuffer_t*);
